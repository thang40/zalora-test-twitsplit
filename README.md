This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

example project of twitsplit where users can post their messages and auto split in chunks.

## Available Scripts

In the project directory, you can run:

### `npm start` or `yarn start`

### `npm test` or `yarn test`
