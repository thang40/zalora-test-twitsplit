import React, { Component } from "react";
import "./App.css";
import { Message } from "./commons/components";
import {
    saveMessage,
    getMessage,
    resolveAvatarUrl,
    getUser,
    rememberUser,
    logout
} from "./services";

class App extends Component {
    state = {
        twit: "",
        messages: [],
        usernameInput: "",
        currentUser: undefined,
        isInit: false
    };
    componentDidMount() {
        this.setState({
            messages: getMessage(),
            currentUser: getUser(),
            isInit: true
        });
    }
    handleNewTwit = () => {
        const { twit, currentUser } = this.state;
        this.setState({
            messages: saveMessage(twit, currentUser)
        });
    };
    handleTwitInputChange = e => {
        this.setState({
            twit: e.target.value
        });
    };
    handleOnUsernameInputChange = e => {
        this.setState({
            usernameInput: e.target.value
        });
    };
    handleSubmitUsername = () => {
        const { usernameInput } = this.state;
        if (usernameInput.trim().length === 0) {
            return;
        }
        this.setState({
            currentUser: usernameInput
        });
        rememberUser(usernameInput);
    };
    handleLogout = () => {
        this.setState({
            currentUser: undefined
        });
        logout();
    };
    renderInputUser = () => {
        const { usernameInput } = this.state;
        return (
            <div>
                <label>Username</label>
                <input
                    onChange={this.handleOnUsernameInputChange}
                    value={usernameInput}
                    type="text"
                />
                <button onClick={this.handleSubmitUsername}>get me in</button>
            </div>
        );
    };
    renderTwitInterface = () => {
        const { messages, currentUser } = this.state;
        return (
            <React.Fragment>
                <div className="mb-10">
                    <span>
                        <strong>[{currentUser}]</strong>
                    </span>
                    <span>
                        {"     "}not you?{" "}
                        <a href="#" onClick={this.handleLogout}>
                            log out
                        </a>
                    </span>
                </div>
                <div className="flex mb-10">
                    <textarea
                        onChange={this.handleTwitInputChange}
                        className="message-sender"
                        placeholder="your twit"
                    />
                    <button onClick={this.handleNewTwit}>Send</button>
                </div>
                <div className="message-history">
                    {messages &&
                        messages.map((m, i) => (
                            <Message
                                key={i}
                                message={m.message}
                                avatarUrl={resolveAvatarUrl(m.username)}
                                userName={m.username}
                            />
                        ))}
                </div>
            </React.Fragment>
        );
    };
    render() {
        const { isInit, currentUser } = this.state;
        return isInit ? (
            <div className="App">
                <header />
                <main>
                    <article>
                        {currentUser ? this.renderTwitInterface() : this.renderInputUser()}
                    </article>
                </main>
            </div>
        ) : null;
    }
}

export default App;
