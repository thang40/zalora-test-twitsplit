import React from "react";

export const Message = ({ message, avatarUrl, userName }) => {
    return (
        <div className="flex">
            <div className="mr-10">
                <img className="avatar" alt={userName} title={userName} src={avatarUrl} />
            </div>
            <div className="message">{message}</div>
        </div>
    );
};
