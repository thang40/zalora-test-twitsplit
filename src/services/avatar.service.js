const BASE_URL = `https://api.adorable.io/avatars/50/`;

export const resolveAvatarUrl = userName => {
    return `${BASE_URL}${userName}`;
};
