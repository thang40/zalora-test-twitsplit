import { splitMessage } from "../utils/splitMessage";

const KEY = "example_twitapp";

export const saveMessage = (message, currentUser) => {
    const messageFromDb = JSON.parse(localStorage.getItem(KEY)) || [];
    try {
        const handledTwit = splitMessage(message);
        let newMessages = [...messageFromDb];
        if (Array.isArray(handledTwit)) {
            handledTwit.reverse().forEach(t => {
                newMessages = [{ message: t, username: currentUser }, ...newMessages];
            });
            localStorage.setItem(KEY, JSON.stringify(newMessages));
            return newMessages;
        } else {
            newMessages = [{ message: handledTwit, username: currentUser }, ...newMessages];
            localStorage.setItem(KEY, JSON.stringify(newMessages));
            return newMessages;
        }
    } catch (error) {
        alert(error);
        return messageFromDb;
    }
};

export const getMessage = () => {
    return JSON.parse(localStorage.getItem(KEY)) || [];
};
