const KEY = "example_twitapp_user";

export const rememberUser = username => {
    try {
        localStorage.setItem(KEY, username);
    } catch (error) {
        localStorage.removeItem(KEY);
    }
};

export const getUser = () => {
    const username = localStorage.getItem(KEY) || undefined;
    return username;
};

export const logout = () => {
    localStorage.removeItem(KEY);
};
