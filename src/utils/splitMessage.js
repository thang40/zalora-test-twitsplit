export const splitMessage = message => {
    const result = resolveIndicators(splitMessageRecursively(message));
    return result;
};

const splitMessageRecursively = (message, hasIndicator = false) => {
    const INVALID_MSG_ERR = "message can not be splitted";
    const MAX_LENGTH = hasIndicator ? 46 : 50;
    let length;
    let trimmedMessage;
    let midIndex;
    let lastIndexOfSpaceFirstHalf;
    let splitResult = [];
    trimmedMessage = message.trim();
    length = trimmedMessage.length;
    if (length <= MAX_LENGTH) {
        return trimmedMessage;
    }
    midIndex = Math.floor(trimmedMessage.length / 2);
    if (trimmedMessage[midIndex] === " ") {
        splitResult.push(...handleLongMessage(trimmedMessage, midIndex));
        return splitResult;
    }
    lastIndexOfSpaceFirstHalf = trimmedMessage.substring(0, midIndex).lastIndexOf(" ");
    if (lastIndexOfSpaceFirstHalf === -1) {
        throw new Error(INVALID_MSG_ERR);
    }
    splitResult.push(...handleLongMessage(trimmedMessage, lastIndexOfSpaceFirstHalf));
    return splitResult;
};

const resolveIndicators = message => {
    const type = typeof message;
    let length;
    if (type === "string") {
        return message;
    }
    length = message.length;
    return message.map((chunkedMessage, index) => {
        const chunkedMessageType = typeof chunkedMessage;
        if (chunkedMessageType !== "string") {
            throw new Error("Invalid param");
        }
        return `${index + 1}/${length} ${chunkedMessage}`;
    });
};

const handleLongMessage = (targetMessage, splitIndex) => {
    let splitResult = [];
    const firstHalf = targetMessage.substring(0, splitIndex);
    const secondHalf = targetMessage.substring(splitIndex);
    const splittedFirstHalf = splitMessageRecursively(firstHalf, true);
    const splittedSecondHalf = splitMessageRecursively(secondHalf, true);
    Array.isArray(splittedFirstHalf)
        ? splitResult.push(...splittedFirstHalf)
        : splitResult.push(splittedFirstHalf);
    Array.isArray(splittedSecondHalf)
        ? splitResult.push(...splittedSecondHalf)
        : splitResult.push(splittedSecondHalf);
    return splitResult;
};
