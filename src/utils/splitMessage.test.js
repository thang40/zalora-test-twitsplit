import { splitMessage } from "./splitMessage";

test("short message", () => {
    expect(splitMessage("a b")).toBe("a b");
});

test("short 50 chars message", () => {
    expect(splitMessage("01234567890123456789012345678901234567890123456789")).toBe(
        "01234567890123456789012345678901234567890123456789"
    );
});

test("long message", () => {
    expect(
        splitMessage(
            "The quick brown fox jumped over the lazy dog. If the dog barked, was it really lazy?"
        )
    ).toEqual([
        "1/2 The quick brown fox jumped over the lazy",
        "2/2 dog. If the dog barked, was it really lazy?"
    ]);
});

test("another long message", () => {
    expect(
        splitMessage(
            `Messages will only be split on whitespace. If the message contains a span of
            non-whitespace characters longer than 50 characters, display an error`
        )
    ).toEqual([
        "1/4 Messages will only be split on",
        "2/4 whitespace. If the message contains a span of",
        "3/4 non-whitespace characters longer",
        "4/4 than 50 characters, display an error"
    ]);
});

test("long message without whitespace", () => {
    function splitNonWhiteSpaceMsg() {
        splitMessage("Thequickbrownfoxjumpedoverthelazydog.Ifthe dog barked, was it really lazy?");
    }
    expect(splitNonWhiteSpaceMsg).toThrowError("message can not be splitted");
});

test("long message over limit with indicator", () => {
    const overLimitWithIndicatorParam = () => {
        splitMessage(
            "01234567890123456789012345678901234567890123456789 01234567890123456789012345678901234567890123456789"
        );
    };
    expect(overLimitWithIndicatorParam).toThrowError("message can not be splitted");
});

test("invalid type of message", () => {
    const numberParam = () => {
        splitMessage(13);
    };
    const noParam = () => {
        splitMessage();
    };
    expect(numberParam).toThrow();
    expect(noParam).toThrow();
});
